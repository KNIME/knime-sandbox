/*
 * ------------------------------------------------------------------------
 *  Copyright by KNIME AG, Zurich, Switzerland
 *  Website: http://www.knime.com; Email: contact@knime.com
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, Version 3, as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 *  Additional permission under GNU GPL version 3 section 7:
 *
 *  KNIME interoperates with ECLIPSE solely via ECLIPSE's plug-in APIs.
 *  Hence, KNIME and ECLIPSE are both independent programs and are not
 *  derived from each other. Should, however, the interpretation of the
 *  GNU GPL Version 3 ("License") under any applicable laws result in
 *  KNIME and ECLIPSE being a combined program, KNIME AG herewith grants
 *  you the additional permission to use and propagate KNIME together with
 *  ECLIPSE with only the license terms in place for ECLIPSE applying to
 *  ECLIPSE and the GNU GPL Version 3 applying for KNIME, provided the
 *  license terms of ECLIPSE themselves allow for the respective use and
 *  propagation of ECLIPSE together with KNIME.
 *
 *  Additional permission relating to nodes for KNIME that extend the Node
 *  Extension (and in particular that are based on subclasses of NodeModel,
 *  NodeDialog, and NodeView) and that only interoperate with KNIME through
 *  standard APIs ("Nodes"):
 *  Nodes are deemed to be separate and independent programs and to not be
 *  covered works.  Notwithstanding anything to the contrary in the
 *  License, the License does not apply to Nodes, you are not required to
 *  license Nodes under the License, and you are granted a license to
 *  prepare and propagate Nodes, in each case even if such Nodes are
 *  propagated with or for interoperation with KNIME.  The owner of a Node
 *  may freely choose the license terms applicable to such Node, including
 *  when such Node is propagated with or for interoperation with KNIME.
 * -------------------------------------------------------------------
 *
 */

package org.knime.base.node.mine.cluster.silhouette;

import static org.knime.core.node.util.CheckUtils.checkSetting;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.apache.commons.lang3.StringUtils;
import org.knime.base.distance.measure.DistmatrixColumnSelectionPanel;
import org.knime.core.data.DataTableSpec;
import org.knime.core.data.StringValue;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeDialogPane;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;
import org.knime.core.node.NotConfigurableException;
import org.knime.core.node.port.PortObjectSpec;
import org.knime.core.node.util.ColumnSelectionPanel;
import org.knime.distance.DistanceMeasurePortSpec;

/**
 * A dialog to get the number of output clusters,
 * the distance function and the linkage type for cluster.
 *
 * @author Alexander Fillbrunn, University of Konstanz
 */
class SilhouetteCoefficientNodeDialog extends NodeDialogPane {

    private final SilhouetteCoefficientSettings m_settings = new SilhouetteCoefficientSettings();

    private final JCheckBox m_useCache = new JCheckBox();

    private final JCheckBox m_ignoreMissingValues = new JCheckBox();

    private final DistmatrixColumnSelectionPanel m_distColumnSelector = new DistmatrixColumnSelectionPanel();

    private final JLabel m_matrixColumnLabel = new JLabel("Distance matrix column   ");

    private boolean m_requiresDistMatrixColumn;

    private final ColumnSelectionPanel m_clusterColSelector = new ColumnSelectionPanel(StringValue.class);

    /**
     * Creates a new <code>NodeDialogPane</code> for silhouette coefficient calculation in order to set the parameters.
     */
    SilhouetteCoefficientNodeDialog() {
        initComponents();
    }

    private void initComponents() {
        JPanel p = new JPanel(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        c.anchor = GridBagConstraints.WEST;
        c.insets = new Insets(2, 2, 2, 2);
        c.gridx = 0;
        c.gridy = 0;

        p.add(m_matrixColumnLabel, c);
        c.gridx = 1;
        p.add(m_distColumnSelector, c);

        c.gridy++;
        c.gridx = 0;
        p.add(new JLabel("Cluster column   "), c);
        c.gridx = 1;
        c.weightx = 1;
        p.add(m_clusterColSelector, c);

        c.weightx = 0;
        c.gridy++;
        c.gridx = 0;
        p.add(new JLabel("Ignore missing values   "), c);
        c.gridx = 1;
        p.add(m_ignoreMissingValues, c);

        addTab("Standard settings", p);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void loadSettingsFrom(final NodeSettingsRO settings, final PortObjectSpec[] specs)
        throws NotConfigurableException {
        m_settings.loadSettingsForDialog(settings);

        DataTableSpec spec = (DataTableSpec)specs[0];
        m_requiresDistMatrixColumn = specs[1] == null;
        m_matrixColumnLabel.setVisible(m_requiresDistMatrixColumn);
        m_distColumnSelector.update((DistanceMeasurePortSpec)specs[1], spec, m_settings.getMatrixColumn());
        m_useCache.setSelected(m_settings.isUseCache());
        m_ignoreMissingValues.setSelected(m_settings.isIgnoreMissing());
        m_clusterColSelector.update((DataTableSpec)specs[0], m_settings.getClusterColumn());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void saveSettingsTo(final NodeSettingsWO settings) throws InvalidSettingsException {
        m_settings.setUseCache(m_useCache.isSelected());
        m_settings.setIgnoreMissing(m_ignoreMissingValues.isSelected());
        m_settings.setMatrixColumn(m_distColumnSelector.getSelectedColumn());
        checkSetting(!m_requiresDistMatrixColumn || StringUtils.isNotEmpty(m_distColumnSelector.getSelectedColumn()),
            "No Distance Matrix column selected.");
        m_settings.setClusterColumn(m_clusterColSelector.getSelectedColumn());
        m_settings.saveSettingsTo(settings);
    }
}
