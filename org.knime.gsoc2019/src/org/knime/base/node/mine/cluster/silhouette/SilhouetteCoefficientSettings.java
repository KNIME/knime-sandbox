/*
 * ------------------------------------------------------------------------
 *
 *  Copyright by KNIME AG, Zurich, Switzerland
 *  Website: http://www.knime.com; Email: contact@knime.com
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, Version 3, as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 *  Additional permission under GNU GPL version 3 section 7:
 *
 *  KNIME interoperates with ECLIPSE solely via ECLIPSE's plug-in APIs.
 *  Hence, KNIME and ECLIPSE are both independent programs and are not
 *  derived from each other. Should, however, the interpretation of the
 *  GNU GPL Version 3 ("License") under any applicable laws result in
 *  KNIME and ECLIPSE being a combined program, KNIME AG herewith grants
 *  you the additional permission to use and propagate KNIME together with
 *  ECLIPSE with only the license terms in place for ECLIPSE applying to
 *  ECLIPSE and the GNU GPL Version 3 applying for KNIME, provided the
 *  license terms of ECLIPSE themselves allow for the respective use and
 *  propagation of ECLIPSE together with KNIME.
 *
 *  Additional permission relating to nodes for KNIME that extend the Node
 *  Extension (and in particular that are based on subclasses of NodeModel,
 *  NodeDialog, and NodeView) and that only interoperate with KNIME through
 *  standard APIs ("Nodes"):
 *  Nodes are deemed to be separate and independent programs and to not be
 *  covered works.  Notwithstanding anything to the contrary in the
 *  License, the License does not apply to Nodes, you are not required to
 *  license Nodes under the License, and you are granted a license to
 *  prepare and propagate Nodes, in each case even if such Nodes are
 *  propagated with or for interoperation with KNIME.  The owner of a Node
 *  may freely choose the license terms applicable to such Node, including
 *  when such Node is propagated with or for interoperation with KNIME.
 * ---------------------------------------------------------------------
 *
 * History
 *   25 Jun 2019 (Alexander): created
 */
package org.knime.base.node.mine.cluster.silhouette;

import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;

/**
 * Settings for the silhouette coefficient node.
 * @author Alexander Fillbrunn
 */
public class SilhouetteCoefficientSettings {

    private static final String CFG_MATRIX_COLUMN = "matrixColumn";
    private static final String CFG_CLUSTER_COLUMN = "clusterColumn";
    private static final String CFG_USE_CACHE = "useCache";
    private static final String CFG_IGNORE_MISSING = "ignoreMissing";

    private String m_matrixColumn;
    private String m_clusterColumn;
    private boolean m_useCache;
    private boolean m_ignoreMissing = true;

    /**
     * @return whether missing values are ignored
     */
    public boolean isIgnoreMissing() {
        return m_ignoreMissing;
    }

    /**
     * Sets whether missing values are ignored.
     * @param ignoreMissing if true, missing values are ignored
     */
    public void setIgnoreMissing(final boolean ignoreMissing) {
        m_ignoreMissing = ignoreMissing;
    }

    /**
     * @return whether calculated distances should be cached
     */
    public boolean isUseCache() {
        return m_useCache;
    }

    /**
     * Sets whether calculated distances should be cached.
     * @param useCache if true, a cache is used during calculation of the silhouette coefficient
     */
    public void setUseCache(final boolean useCache) {
        m_useCache = useCache;
    }

    /**
     * @return the name of the column with the distance matrix.
     */
    public String getMatrixColumn() {
        return m_matrixColumn;
    }

    /**
     * Sets the name of the column with the distance matrix.
     * @param matrixColumn the column name
     */
    public void setMatrixColumn(final String matrixColumn) {
        m_matrixColumn = matrixColumn;
    }

    /**
     * @return the name of the column with the cluster assignment
     */
    public String getClusterColumn() {
        return m_clusterColumn;
    }

    /**
     * Sets the name of the column with the cluster assignment.
     * @param clusterColumn the column name
     */
    public void setClusterColumn(final String clusterColumn) {
        m_clusterColumn = clusterColumn;
    }

    /**
     * Loads the settings from the node settings object.
     *
     * @param settings the node settings
     *
     * @throws InvalidSettingsException if one of the settings is missing
     */
    public void loadSettings(final NodeSettingsRO settings)
            throws InvalidSettingsException {
        m_matrixColumn = settings.getString(CFG_MATRIX_COLUMN);
        m_clusterColumn = settings.getString(CFG_CLUSTER_COLUMN);
        m_useCache = settings.getBoolean(CFG_USE_CACHE);
        m_ignoreMissing = settings.getBoolean(CFG_IGNORE_MISSING);
    }

    /**
     * Loads the settings from the node settings object.
     *
     * @param settings the node settings
     */
    public void loadSettingsForDialog(final NodeSettingsRO settings) {
        m_matrixColumn = settings.getString(CFG_MATRIX_COLUMN, null);
        m_clusterColumn = settings.getString(CFG_CLUSTER_COLUMN, null);
        m_useCache = settings.getBoolean(CFG_USE_CACHE, false);
        m_ignoreMissing = settings.getBoolean(CFG_IGNORE_MISSING, true);
    }

    /**
     * Saves the settings to the node settings object.
     *
     * @param settings the node settings
     */
    public void saveSettingsTo(final NodeSettingsWO settings) {
        settings.addString(CFG_MATRIX_COLUMN, m_matrixColumn);
        settings.addString(CFG_CLUSTER_COLUMN, m_clusterColumn);
        settings.addBoolean(CFG_USE_CACHE, m_useCache);
        settings.addBoolean(CFG_IGNORE_MISSING, m_ignoreMissing);
    }
}
