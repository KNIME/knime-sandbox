/*
 * ------------------------------------------------------------------------
 *  Copyright by KNIME AG, Zurich, Switzerland
 *  Website: http://www.knime.com; Email: contact@knime.com
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, Version 3, as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 *  Additional permission under GNU GPL version 3 section 7:
 *
 *  KNIME interoperates with ECLIPSE solely via ECLIPSE's plug-in APIs.
 *  Hence, KNIME and ECLIPSE are both independent programs and are not
 *  derived from each other. Should, however, the interpretation of the
 *  GNU GPL Version 3 ("License") under any applicable laws result in
 *  KNIME and ECLIPSE being a combined program, KNIME AG herewith grants
 *  you the additional permission to use and propagate KNIME together with
 *  ECLIPSE with only the license terms in place for ECLIPSE applying to
 *  ECLIPSE and the GNU GPL Version 3 applying for KNIME, provided the
 *  license terms of ECLIPSE themselves allow for the respective use and
 *  propagation of ECLIPSE together with KNIME.
 *
 *  Additional permission relating to nodes for KNIME that extend the Node
 *  Extension (and in particular that are based on subclasses of NodeModel,
 *  NodeDialog, and NodeView) and that only interoperate with KNIME through
 *  standard APIs ("Nodes"):
 *  Nodes are deemed to be separate and independent programs and to not be
 *  covered works.  Notwithstanding anything to the contrary in the
 *  License, the License does not apply to Nodes, you are not required to
 *  license Nodes under the License, and you are granted a license to
 *  prepare and propagate Nodes, in each case even if such Nodes are
 *  propagated with or for interoperation with KNIME.  The owner of a Node
 *  may freely choose the license terms applicable to such Node, including
 *  when such Node is propagated with or for interoperation with KNIME.
 * -------------------------------------------------------------------
 *
 */
package org.knime.base.node.mine.cluster.silhouette;

import static org.knime.core.node.util.CheckUtils.checkSettingNotNull;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import org.knime.base.util.flowvariable.FlowVariableProvider;
import org.knime.core.data.DataCell;
import org.knime.core.data.DataColumnSpec;
import org.knime.core.data.DataColumnSpecCreator;
import org.knime.core.data.DataRow;
import org.knime.core.data.DataTableSpec;
import org.knime.core.data.DataTableSpecCreator;
import org.knime.core.data.RowKey;
import org.knime.core.data.StringValue;
import org.knime.core.data.append.AppendedColumnRow;
import org.knime.core.data.container.DataContainer;
import org.knime.core.data.def.DefaultRow;
import org.knime.core.data.def.DoubleCell;
import org.knime.core.node.BufferedDataTable;
import org.knime.core.node.CanceledExecutionException;
import org.knime.core.node.ExecutionContext;
import org.knime.core.node.ExecutionMonitor;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeModel;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;
import org.knime.core.node.port.PortObject;
import org.knime.core.node.port.PortObjectSpec;
import org.knime.core.node.port.PortType;
import org.knime.distance.DistanceMeasure;
import org.knime.distance.DistanceMeasurePortObject;
import org.knime.distance.DistanceMeasurePortSpec;
import org.knime.distance.util.MiscUtil;
import org.knime.distmatrix.calculate.measure.MatrixDistanceConfig;
import org.knime.distmatrix.type.DistanceVectorDataValue;

/**
 * Implements calculation of the silhouette coefficient.
 *
 * @author Alexander Fillbrunn, University of Konstanz
 */
public class SilhouetteCoefficientNodeModel extends NodeModel implements FlowVariableProvider  {

    /**
     *
     */
    private static final String MISSING_VALUE_MSG = "The cluster column contains missing values.";

    private SilhouetteCoefficientSettings m_settings = new SilhouetteCoefficientSettings();

    /**
     * Creates a new hierarchical clustering model.
     */
    public SilhouetteCoefficientNodeModel() {
        super(new PortType[]{BufferedDataTable.TYPE, DistanceMeasurePortObject.TYPE_OPTIONAL},
            new PortType[]{BufferedDataTable.TYPE, BufferedDataTable.TYPE});
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected PortObject[] execute(final PortObject[] data, final ExecutionContext exec) throws Exception {
        BufferedDataTable inputData = (BufferedDataTable)data[0];

        if (inputData.size() == 0) {
            throw new InvalidSettingsException("Cannot calculate the silhouette coefficients in an empty table");
        }

        // Select the distance measure depending on whether the optional port is connected or not
        DistanceMeasure<?> rowDist;
        if (data[1] == null) {
            // Not connected --> Use a distance matrix column
            String matrixColumn = m_settings.getMatrixColumn();
            rowDist = new MatrixDistanceConfig(matrixColumn).createDistanceMeasure(inputData.getDataTableSpec(), this);
        } else {
            // Connected --> Use the distance measure from the port
            rowDist = ((DistanceMeasurePortObject)data[1]).createDistanceMeasure(inputData.getDataTableSpec(), this);
        }

        int clusterCol = inputData.getDataTableSpec().findColumnIndex(m_settings.getClusterColumn());
        Aggregator overallAgg = new Aggregator();

        DataContainer dc = exec.createDataContainer(createFullTableSpec(inputData.getDataTableSpec()));
        // Silhouette coefficient is calculated for each data point
        for (DataRow row1 : inputData) {
            // We need to find the cluster where the average distance between its data points and
            // the current data point is minimal, excluding the current data points assigned cluster.
            DataCell selfClusterCell = row1.getCell(clusterCol);
            if (selfClusterCell.isMissing()) {
                if (m_settings.isIgnoreMissing()) {
                    continue;
                } else {
                    throw new InvalidSettingsException(MISSING_VALUE_MSG);
                }
            }
            String selfCluster = selfClusterCell.toString();
            HashMap<String, Aggregator> daMap = new HashMap<String, Aggregator>();

            // Calculate distances to other clusters
            for (DataRow row2 : inputData) {
                // The distance of a point to itself is not included.
                // Note: Not all row implementations implement equals(), so we compare RowKeys instead.
                if (!row1.getKey().equals(row2.getKey())) {
                    DataCell otherClusterCell = row2.getCell(clusterCol);
                    if (otherClusterCell.isMissing()) {
                        if (m_settings.isIgnoreMissing()) {
                            continue;
                        } else {
                            throw new InvalidSettingsException(MISSING_VALUE_MSG);
                        }
                    }
                    String otherCluster = otherClusterCell.toString();
                    Aggregator agg = daMap.computeIfAbsent(otherCluster, s -> new Aggregator());
                    agg.addDistance(rowDist.computeDistance(row1, row2));
                }
            }

            // Find closest other cluster
            Aggregator self = daMap.get(selfCluster);
            double selfDist = self != null ? self.getMean() : 0;
            double nextDist = Double.MAX_VALUE;
            for (String cluster : daMap.keySet()) {
                if (!cluster.equals(selfCluster)) {
                    double d = daMap.get(cluster).getMean();
                    if (d < nextDist) {
                        nextDist = d;
                    }
                }
            }

            // Calculate the silhouette coefficient for the data point
            double silhouette = (nextDist - selfDist) / Math.max(nextDist, selfDist);
            DataCell silCell = new DoubleCell(silhouette);
            dc.addRowToTable(new AppendedColumnRow(row1, silCell));
            overallAgg.addDistance(silhouette);
        }
        dc.close();

        // Create table for overall silhouette coefficient
        DataContainer stats = exec.createDataContainer(createStatsSpec());
        stats.addRowToTable(new DefaultRow(new RowKey("Silhouette Coefficient"), new DoubleCell(overallAgg.getMean())));
        stats.close();

        return new PortObject[]{(BufferedDataTable)dc.getTable(), (BufferedDataTable)stats.getTable()};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected PortObjectSpec[] configure(final PortObjectSpec[] inSpecs) throws InvalidSettingsException {
        DataTableSpec spec = (DataTableSpec)inSpecs[0];

        if (inSpecs[1] == null) {
            String matrixColumn = m_settings.getMatrixColumn();
            if (matrixColumn == null) {
                DataColumnSpec matrixSpec =
                    checkSettingNotNull(MiscUtil.getFirstCompatibleColumn(spec, DistanceVectorDataValue.class),
                        "The matrix column is not set and no compatible column was found in the input table.");
                matrixColumn = matrixSpec.getName();
                m_settings.setMatrixColumn(matrixColumn);
                setWarningMessage(String.format("Guessing distance matrix column: '%s'", matrixColumn));
            } else if (!spec.containsName(matrixColumn)) {
                throw new InvalidSettingsException(String.format(
                    "A distance matrix column with the name '%s' does not exist in the input table.", matrixColumn));
            }
            new MatrixDistanceConfig(matrixColumn).getSpec().validate(spec);
        } else {
            ((DistanceMeasurePortSpec)inSpecs[1]).validate(spec);
        }

        String clusterColumn = m_settings.getClusterColumn();
        if (clusterColumn == null) {
            DataColumnSpec clusterSpec =
                checkSettingNotNull(MiscUtil.getFirstCompatibleColumn(spec, StringValue.class),
                    "The cluster column is not set and no compatible string column was found in the input table.");
            clusterColumn = clusterSpec.getName();
            m_settings.setClusterColumn(clusterColumn);
            setWarningMessage(String.format("Guessing cluster column: '%s'", clusterColumn));
        } else if (!spec.containsName(clusterColumn)) {
            throw new InvalidSettingsException(String.format(
                "A string column with the name '%s' does not exist in the input table.", clusterColumn));
        }

        return new PortObjectSpec[]{createFullTableSpec(spec), createStatsSpec()};
    }

    private static DataTableSpec createFullTableSpec(final DataTableSpec inSpec) {
        DataTableSpecCreator appSpecCreator = new DataTableSpecCreator();
        appSpecCreator.addColumns(new DataColumnSpecCreator("silhouetteCoefficient", DoubleCell.TYPE).createSpec());
        return new DataTableSpec(inSpec, appSpecCreator.createSpec());
    }

    private static DataTableSpec createStatsSpec() {
        DataTableSpecCreator specCreator = new DataTableSpecCreator();
        specCreator.addColumns(new DataColumnSpecCreator("Value", DoubleCell.TYPE).createSpec());
        return specCreator.createSpec();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void loadValidatedSettingsFrom(final NodeSettingsRO settings) throws InvalidSettingsException {
        m_settings.loadSettings(settings);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void saveSettingsTo(final NodeSettingsWO settings) {
        m_settings.saveSettingsTo(settings);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void validateSettings(final NodeSettingsRO settings) throws InvalidSettingsException {
        SilhouetteCoefficientSettings s = new SilhouetteCoefficientSettings();
        s.loadSettings(settings);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void loadInternals(final File nodeInternDir, final ExecutionMonitor exec)
        throws IOException, CanceledExecutionException {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void saveInternals(final File nodeInternDir, final ExecutionMonitor exec)
        throws IOException, CanceledExecutionException {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void reset() {
    }

    private class Aggregator {
        private int m_count = 0;

        private double m_sum = 0;

        /**
         * Adds a distance to the aggregation.
         *
         * @param d the distance to add
         */
        public void addDistance(final double d) {
            m_sum += d;
            m_count++;
        }

        /**
         * @return the mean of all added distances
         */
        public double getMean() {
            return m_sum / m_count;
        }
    }
}
